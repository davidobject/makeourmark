function API() {

	var LIMIT = 15;
	var VID_URL = 'https://levismakeourmark.thismoment.com/v4/api/gallery/get.json?environment=1&gallery_id=7&category_id=22&total=' + LIMIT;
	var PHOTO_URL = 'https://levismakeourmark.thismoment.com/v4/api/gallery/get.json?environment=1&gallery_id=7&category_id=21&total=' + LIMIT;
	
	var searchPhotosURL = PHOTO_URL;
	var searchVidsURL = VID_URL;
	
	var _totalVids;
	var _totalPhotos;

	var _this = this;
	var _photos;
	var _vids;
	var _allGrams = [];
	var _searchResults = [];
	var _uids = {};

	this.init = function() {
		_allGrams = [];
		this.loadAllData();
		$(this).on('loadalldata', function() {
			$(this).trigger('oninit');
		});
	};

	this.loadAllData = function() {
		this.loadAllPhotos();
		$(this).on('loadallphotos', function() {
			this.loadAllVids();
			$(this).off('loadallphotos');
		});
		$(this).on('loadallvids', function() {

			$(this).off('loadallvids loadallphotos');
			$(this).trigger('loadalldata');
		});
	};

	this.loadAllPhotos = function() {
		if (!_photos)
			_photos = [];
		$.getJSON(PHOTO_URL, $.proxy(onLoadAllPhotos, this));
	};

	function onLoadAllPhotos(data) {
		if (!_totalPhotos)
			_totalPhotos = data.paging.total_content;
		PHOTO_URL = data.paging.next;

		parseResults(data.results, _photos);
		$(_this).trigger('loadallphotos');

		if (_photos.length < _totalPhotos)
			setTimeout(this.loadAllPhotos, 10000);

	};

	this.loadAllVids = function() {
		if (!_vids)
			_vids = [];
		$.getJSON(VID_URL, $.proxy(onLoadAllVids, this));
	};

	function onLoadAllVids(data) {
		if (!_totalVids)
			_totalVids = data.paging.total_content;
		VID_URL = data.paging.next;

		parseResults(data.results, _vids);
		$(_this).trigger('loadallvids');

		if (_vids.length < _totalVids)
			setTimeout(this.loadAllVids, 10000)
	};

	this.search = function(s) {
		_searchResults = [];
		$.getJSON(searchPhotosURL + '&query=' + s, $.proxy(function(data) {

			parseResults(data.results, _searchResults);
			$.getJSON(searchVidsURL + '&query=' + s, $.proxy(function(data) {
				parseResults(data.results, _searchResults);
				$(_this).trigger('onsearch');
			}));
		}, this));
	};

	this.clearSearch = function() {
		_searchResults = [];
	};

	function parseResults(results, a) {

		for (var i = 0; i < results.length; i++) {
			var r = results[i];
			var g = new Gram;

			g.id = r.stream_post_id;
			g.time = r.add_date;
			g.title = r.title;
			g.description = r.description;
			g.keywords = r.keywords;
			g.name = r.user_info.name;
			g.username = r.user_info.screen_name;
			g.avatar = r.user_info.avatar_url;
			g.imgURLS = {};
			g.imgURLS.thumb = r.media[0].thumb;
			g.imgURLS.large = r.media[0].large;
			g.imgURLS.medium = r.media[0].medium;
			g.imgURLS.small = r.media[0].small;
			g.file = r.media[0].orig_file;
			g.vid = r.media[0].video_url;

			if (!_uids[g.id]) {
				_allGrams.push(g);
				_uids[g.id] = true;
			}
			a.push(g);
		}
	}
	;

	/* GETTER/SETTERS */
	this.allGrams = function() {
		return _allGrams;
	};
	this.photos = function() {
		return _photos;
	};
	this.vids = function() {
		return _vids;
	};
	this.searchResults = function() {
		return _searchResults;
	};
}
function Main() {

	//main constants
	var MARGIN_LEFT = 20;
	var MARGIN_RIGHT = 20;
	var MARGIN_TOP = 102;
	var MARGIN_BOT = 20;
	var MAX_CAM_ROT = (SAFARI || IE10) ? 20 : 30;
	var IMG_QUALITY = 'thumb';
	var SEARCH_PROMPT = 'SEARCH BY USERNAME'; 

	var _this = this;

	//viewport & stage
	var _stageW = 0;
	var _stageH = 0;
	var _viewportW = 0;
	var _viewportH = 0;

	//quads & tiles
	var _quads = {};
	var _tiles = [];
	var _tilePool = [];
	var _activeTile;

	//holder divs
	var _viewport = $('#viewport');
	var _mainDiv = $('#main'); //cam
	var _tilesDiv = $('#tiles');
	var _modal = $('#modal');
	var _modalContent;

	//position & velocity
	var _dragStart = new Vec2(0, 0);
	var _tilesOrigin = new Vec2(0, 0);
	var _tilesVel = new Vec2(0, 0);
	var _tilesPos = new Vec2(0, 0);
	var _tilesTo = new Vec2(0, 0);
	
	//flags =/
	var _tilesEnabled = false;
	var _activated = false;
	
	//z-sort
	var _lastZIndex = 0;
	var _zDir = -1;
	
	//animation
	var _transIndex = 0; //init
	var _moving = false;
	
	//sound
	var _ambient;
	var _wind;
	this.windVol = 100;

	//api
	var _api = new API();
	
	//search
	var _searching = false;

	//debug
	var _stats;
	var _maxChildren = 0;

	//initialize
	this.init = function() {
		
		showLoading();

		if (DEBUG) {
			_stats = new Stats();
			_stats.setMode(0);
			_stats.domElement.style.position = 'absolute';
			_stats.domElement.style.left = '0px';
			_stats.domElement.style.top = '0px';
			$('body').append(_stats.domElement);
		}

		_api.init();
		$(_api).on('oninit', onInitAPI);
	};

	//preload initial images
	function onInitAPI() {

		var p = new Preloader();
		p.preloadGrams(_api.allGrams(), IMG_QUALITY);
		$(p).on('preloaded', onPreloadGrams);
		
	};
	
	//preload overlays
	function onPreloadGrams() {
		
		var p = new Preloader();
		var urls = [];
		for (var i=0; i<=30; i++) {
			urls.push('assets/img/overlays/'+i+'.jpg');
		}
		p.preloadURLS(urls);
		$(p).on('preloaded', onPreloadInitialImages);
	};
	
	//init layout, listeners & start
	function onPreloadInitialImages() {
		
		//init pool
		for (var i=1; i<=MAX_SIZE; i++) {
			_tilePool[i] = [];
		}
		
		//init layout
		$(window).resize(onResize);
		onResize();

		TweenMax.set(_mainDiv, {rotationX: 0, rotationY: 0, transformPerspective: 1000, x: 0, y: 0, z: 0});
		TweenMax.set(_tilesDiv, {x: 0, y: 0, z:0, perspective:400});
		
		//trans in
		draw();
		hideLoading();
		setTimeout(function() {

			//start!
			initSearch();
			startEnterFrame();
			startMouseTracking();
//			enableDrag();
			enableMovement();
			_tilesEnabled = true;
			if (PLAY_SOUND) setTimeout(startSound, 500);
			
			_transIndex = 3;
			
			$(window).on('enterframe', draw);
		}, 5500);
		
		TweenMax.from(_mainDiv, 6, {
			rotationX:randRange(-40,40),
			rotationY:randRange(-70,70),
			rotationZ:randRange(-70,70),
			ease:Strong.easeOut
		});
	};
	
	function startSound() {
		soundManager.setup({
			url: 'assets/swf/sound_manager/',
			preferFlash: false,
			debugMode: false,
			onready: function() {
//				_ambient = soundManager.createSound({
//					url: 'assets/mp3/ambient.mp3',
//					id: 'ambient'
//				});
//				_ambient.play({
//					loops: 10000000
//				});
				
				_wind = soundManager.createSound({
					url: 'assets/mp3/wind.mp3',
					id: 'wind'
				});
				_wind.play({
					loops: 10000000
				});
			}
		});
	};

	//main draw function
	function draw() {
		if (_stats) _stats.begin();
		
		if (!_activated) {
			updatePerspective();
			updateGrid();
			updateTiles();
		}
		rotateCam();
		updateVol();
		
		if (_stats) _stats.end();
	};
	
	function updateVol() {
		soundManager.setVolume('wind', Math.round(_this.windVol));
	}

	//main grid building function
	function updateGrid() {

		//1. remove out of bound tiles
		for (var i = 0; i < _tiles.length; i++) {
			var t = _tiles[i];
			
			if (t.transitioning) continue;
			
			var doRemove = false;

			var tX = getX(t.div) + getX(_tilesDiv);
			var tY = getY(t.div) + getY(_tilesDiv);

			//out of bounds
			if (tX > _viewportW || tX + t.width < 0 || tY > _viewportH || tY + t.height < 0) {
				removeTile(t);
				i--;
				continue;
			}
		}

		//2. find all visible empty quads within bounds
		var tX = getX(_tilesDiv);
		var tY = getY(_tilesDiv);
		var left = -Math.ceil(tX / QUAD_W) * QUAD_W;
		var top = -Math.ceil(tY / QUAD_H) * QUAD_H;
		var cols = Math.ceil(((tX <= 0 ? Math.abs(tX % QUAD_W) : (QUAD_W - (tX % QUAD_W))) + _viewportW) / QUAD_W);
		var rows = Math.ceil(((tY <= 0 ? Math.abs(tY % QUAD_H) : (QUAD_H - (tY % QUAD_H))) + _viewportH) / QUAD_H);
		
		var x = left;
		var y = top;
		var emptyQuads = [];

		//iterate over visible quads, create new quad if it does not exist, store empty visible quads
		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				var pt = new Vec2(x, y);
				var q = getQuad(pt);

				//store empty quads
				if (!q.tile) {
					emptyQuads.push(q);
				}
				x += QUAD_W;
			}
			x = left;
			y += QUAD_H;
		}
		emptyQuads = shuffleArray(emptyQuads); //shuffle quad order

		//3. generate randomized tile pt and size for empty visible quads
		for (var k = 0; k < emptyQuads.length; k++) {
			var q = emptyQuads[k];

			if (q.tile)
				continue;

			//choose rand size
//			var size = randInt(1, MAX_SIZE);
			var size = MAX_SIZE;
			var tilePt;

			if (size > 1) {
				var currentSize = 1;

				//random expand direction
				var dirX = (Math.random() > .5) ? -1 : 1;
				var dirY = (Math.random() > .5) ? -1 : 1;

				for (var s = 2; s <= size; s++) {
					var isOccupied = false;
					for (var i = 0; i < s; i++) {
						for (var j = 0; j < s; j++) {
							var pt = new Vec2(q.pt.x + dirX * QUAD_W * i, q.pt.y + dirY * QUAD_H * j);

							if (getQuad(pt).tile) {
								isOccupied = true;
								break;
							}
						}
						if (isOccupied)
							break;
					}
					if (isOccupied)
						break;
					else
						currentSize++;
				}

				//determine size and pt based on expand direction
				size = currentSize;
				if (size > 1) {
					if (dirX === -1 && dirY === -1) {
						tilePt = new Vec2(q.pt.x - QUAD_W * (size - 1), q.pt.y - QUAD_H * (size - 1));
					} else if (dirX === -1 && dirY === 1) {
						tilePt = new Vec2(q.pt.x - QUAD_W * (size - 1), q.pt.y);
					} else if (dirX === 1 && dirY === -1) {
						tilePt = new Vec2(q.pt.x, q.pt.y - QUAD_H * (size - 1));
					} else {
						tilePt = q.pt;
					}
				} else {
					tilePt = q.pt;
				}
			} else {
				size = 1;
				tilePt = q.pt;
			}

			//create new tile at pt
			addTile(tilePt, size);
		}
	};

	//return existing quad, or new quad if it does not exist at pt
	function getQuad(pt) {
		var q;
		if (_quads[pt]) {
			q = _quads[pt]; //already exists
		} else {
			q = _quads[pt] = new Quad(pt, null); //create
		}

		return q;
	};
	
	function updateTiles() {
		
	};

	//add tile to grid
	function addTile(pt, size) {
		var t; var d; //tile & tile's div
		var w = size * QUAD_W;
		var h = size * QUAD_H;

		//get tile from pool
		if (_tilePool[size].length > 0) {
			t = _tilePool[size].pop();
			t.pt = pt;
			d = t.div;
			
			createTileImage(t);
			d.css({'visibility':'visible'});
			
		} else { //create a new tile, add it to the pool
			t = createTile(pt, size);
			d = t.div;
			
			_tilesDiv.append(d);
		
		}

		//start transition
		var yRatio = 1 - ((pt.y + h / 2 + getY(_tilesDiv)) / _viewportH);
		if (yRatio > 1)
			yRatio = 1;
		else if (yRatio < 0)
			yRatio = 0;
		var xRatio = 1 - ((pt.x + w / 2 + getX(_tilesDiv)) / _viewportW);
		if (xRatio > 1)
			xRatio = 1;
		else if (xRatio < 0)
			xRatio = 0;

		var tX = Math.round(yRatio * 100) + '%';
		var tY = Math.round(xRatio * 100) + '%';

		//position tile
		TweenMax.set(d, {
			x: pt.x, 
			y: pt.y, 
			z: 0, 
			rotationX: 0, 
			rotationY: 0, 
			rotationZ: 0, 
			transformOrigin: tY + ' ' + tX,
			alpha:1,
			scaleX:1,
			scaleY:1
		});
		
		_lastZIndex+=_zDir;
		d.css({'z-index':_lastZIndex});
		
		//trans in
		transInTile(t, xRatio, yRatio, tX, tY);

		//associate quads with tile
		for (var i = 0; i < size; i++) {
			for (var j = 0; j < size; j++) {
				var q = getQuad(new Vec2(pt.x + i * QUAD_W, pt.y + j * QUAD_H));
				q.tile = t;
				t.quads.push(q);
			}
		}

		//add tile to _tiles stack
		_tiles.push(t);
		
		if (DEBUG && _maxChildren < _tilesDiv.children().length) {
			_maxChildren = _tilesDiv.children().length;
		}
	};

	//clear quads, hide tile
	function removeTile(t, killit) {
		//remove from _tiles stack
		for (var i = 0; i < _tiles.length; i++) {
			if (_tiles[i] === t) {
				_tiles.splice(i, 1);
				break;
			}
		}

		//remove from associated quads
		for (i = 0; i < t.quads.length; i++) {
			t.quads[i].tile = null;
		}
		t.quads = [];
		
		TweenMax.killTweensOf(t.div);
		
		if (killit) {
			t.div.remove();
		} else {
			//hide it behind camera
			TweenMax.set(t.div, {z:10000});
			_tilePool[t.size].push(t);
		}
	};

	
	//return a new tile object of given size
	function createTile(pt, size) {
		var t; var d; //tile & tile's div
		
		var w = size * QUAD_W;
		var h = size * QUAD_H;

		d = $('<div class="tile">');
		t = new Tile(pt, size, d);
		TweenMax.set(d, {x: pt.x, y: pt.y, z: 0, width: w, height: h});
		
		createTileImage(t);
		
		//tile overlay
		var b = t.overlay = $('<div>');
		d.append(b);
		b.css({'background-image':'url("assets/img/overlays/'+randInt(0,30)+'.png")'});
		TweenMax.set(b, {
			width: QUAD_W * size, 
			height: QUAD_H * size, 
			alpha: .92,
			scaleX:1.01,
			scaleY:1.01,
			'backgroundSize': w + 'px ' + h + 'px',
			'backgroundPosition': 0 + 'px ' + 0 + 'px'
		});
		
		//mouse handlers
		d.on('mouseover mouseout click', $.proxy(tileMouseHandler, this, t));
		d.css({'cursor': 'pointer'});

		return t;
	};
	
	//set random image as tile div's bg
	function createTileImage(t) {
		//select random image from all grams
		var d = t.div;
		var r;	var gram;
		if (_searching && _api.searchResults().length > 0) {
			r = randInt(0, _api.searchResults().length - 1);
			gram = _api.searchResults()[r];
		} else {
			r = randInt(0, _api.allGrams().length - 1);
			gram = _api.allGrams()[r];
		}
		t.gram = gram;
		d.css({'background-image': 'url("' + gram.imgURLS[IMG_QUALITY] + '")'});

		setRandomImageSize(t);
	};
	
	function setRandomImageSize(t, time) {
		//randomly zoom img
		var size = t.size;
		var randScale = 1; //size < 3 ? randRange(1, 4) : 1;
		var w = QUAD_W * size * randScale;
		var h = QUAD_H * size * randScale;
		var x = -randInt(0, (QUAD_W * size * randScale) - (QUAD_W * size));
		var y = -randInt(0, (QUAD_H * size * randScale) - (QUAD_H * size));
		
		TweenMax.to(t.div, time, {
			'backgroundSize': w + 'px ' + h + 'px',
			'backgroundPosition': x + 'px ' + y + 'px'
		});
	}
	
	//tile roll & click handlers
	function tileMouseHandler(t, e) {
		switch (e.type) {
			case 'mouseover':
				if (!_tilesEnabled) return;
				TweenMax.to(t.overlay, .25, {alpha:0});
				TweenMax.set(t.div, {transformOrigin:'50% 50%'});
				TweenMax.to(t.div, .25, {
					scaleX:1, 
					scaleY:1
				});
				t.div.css({'z-index':'8888'});
				break;
			case 'mouseout':
				if (!_tilesEnabled) return;
				TweenMax.to(t.overlay, .5, {alpha:.92});
				TweenMax.to(t.div, .25, {
					scaleX:1, 
					scaleY:1
				});
				t.div.css({'z-index':'0'});
				break;
			case 'click':
				if (!_tilesEnabled) return;
				activateTile(t);
				break;
		}
	};
	
	function activateTile(t) {
		_tilesEnabled = false;
		_activated = true;
		disableMovement();
		var dX = -(getX(_tilesDiv) + getX(t.div)) + (_viewportW/2) - t.width/2;
		var dY = -(getY(_tilesDiv) + getY(t.div)) + (_viewportH/2) - t.height/2;
		TweenMax.to(_tilesDiv, 1, {x:getX(_tilesDiv)+dX, y:getY(_tilesDiv)+dY});
		
		var sX = 500/t.width;
		var sY = 610/t.height;
		
		TweenMax.set(t.overlay, {alpha:0});
		TweenMax.to(t.overlay, 1, {alpha:1});
		TweenMax.to(t.div, 1.4, {rotationY:360, transformOrigin:'50% 50% 50%'});
		TweenMax.to(t.overlay, 1.4, {
			scaleX:sX, 
			scaleY:sY, 
			delay:1.1, 
			ease:Strong.easeOut, 
			onComplete:showModal, 
			onCompleteParams:[t.gram]
		});
		for (var i=0; i<_tiles.length; i++) {
			var t0 = _tiles[i];
			if (t0 !== t) {
				TweenMax.to(t0.div, randRange(1,3), 
				{
					rotationX:randRange(-180,180),
					rotationY:randRange(-180,180),
					z:randRange(-2000,-500),
					alpha:0
				});
			}
		};
		t.overlay.css({'background-image':'none', 'background-color':'white'});
		t.div.css({'z-index':'9999', overflow:'visible'});
		_activeTile = t;
	};

	function updatePerspective() {
		//make sure origin is always centered
		var transOrigin = -Math.round(getX(_tilesDiv) - _viewportW / 2) + 'px ' + -Math.round(getY(_tilesDiv) - _viewportH / 2) + 'px';
		
		_tilesDiv.css({
			'perspective-origin':transOrigin,
			'-webkit-perspective-origin':transOrigin,
			'-moz-perspective-origin':transOrigin
		});
	};

	// calc tiles velocity and rotate camera accordingly
	function rotateCam() {

		var dX = getX(_tilesDiv) - _tilesPos.x;
		var dY = getY(_tilesDiv) - _tilesPos.y;

		_tilesVel.x = dX;
		_tilesVel.y = dY;
		_tilesPos.x = getX(_tilesDiv);
		_tilesPos.y = getY(_tilesDiv);

		var rX = Math.round(_tilesVel.y * 2);
		if (rX > MAX_CAM_ROT)
			rX = MAX_CAM_ROT;
		if (rX < -MAX_CAM_ROT)
			rX = -MAX_CAM_ROT;

		var rY = Math.round(-_tilesVel.x * 2);
		if (rY > MAX_CAM_ROT)
			rY = MAX_CAM_ROT;
		if (rY < -MAX_CAM_ROT)
			rY = -MAX_CAM_ROT;
		
		var max = Math.max(Math.abs(rY), Math.abs(rX));
		var tilt = max/MAX_CAM_ROT;
		
		TweenMax.to(_this, 2.5, {windVol:(tilt*85)});

		var rZ = 0;//_tilesVel.y / 5;

		TweenMax.to(_mainDiv, 2.5, {rotationX: rX, rotationY: rY, rotationZ: rZ, ease: Strong.easeOut, overwrite: true});
	};	
	
	//enable movement based on mouse
	function enableMovement() {
		
		if (_moving) return;
		
		$(window).on('enterframe', moveGrid);
		$('body').on('mouseleave', onLeaveWin);
		_moving = true;
	};
	
	function disableMovement() {
		$(window).off('enterframe', moveGrid);
		_moving = false;
	};
	
	function tweenToNewScreen() {
		disableMovement();
		
		TweenMax.to(_tilesDiv, 3, {x:getX(_tilesDiv)+_viewportW+100, onComplete:function() {
			$(_this).trigger('tweentonewscreen');
		}});
	};
	
	function onLeaveWin() {
		$('body').off('mouseleave', onLeaveWin);
		
		if (_moving) {
			disableMovement();
			$('body').on('mouseenter', onEnterWin);
		}
	};
	
	function onEnterWin() {
		if (!_searching && !_modalContent) enableMovement();
	};
	
	//move tiles based on mouse position
	function moveGrid(dontTween) {
		var dX = -(mouse.x - _stageW/2)/(_stageW/2);
		var dY = -(mouse.y - _stageH/2)/(_stageH/2);	
		var d = Math.sqrt(dX*dX+dY*dY);
		
		if (d < .45) {
			dX*=.15;
			dY*=.15;
		} else if (d < .7) {
			dX*=.5;
			dY*=.5;
		} else {
			
		}
		
		if (!isNaN(dX) && !isNaN(dY)) {
			_tilesTo.x += dX*45;
			_tilesTo.y += dY*45;
		}
		
		updatePerspective();
		TweenMax.to(_tilesDiv, .6, {x:_tilesTo.x, y:_tilesTo.y});
	};
	
	//allow dragging
	function enableDrag() {
		_viewport.on('mousedown', dragHandler);
	};

	//prevent dragging
	function disableDrag() {
		_viewport.off('mousedown', dragHandler);
		$(window).off('mouseup', dragHandler);
	};

	//drag mouse handler
	function dragHandler(e) {
		switch (e.type) {
			case 'mousedown':
				e.preventDefault();
				_dragStart = new Vec2(mouse.x, mouse.y);
				_tilesOrigin = new Vec2(getX(_tilesDiv), getY(_tilesDiv));

				$(window).on('mouseup', dragHandler);
				$(window).on('enterframe', drag);
				break;

			case 'mouseup':
				$(window).off('mouseup', dragHandler);
				$(window).off('enterframe', drag);
				drag(true);
				break;
		}
	};

	//do drag
	function drag(released) {
		var dX = (mouse.x - _dragStart.x) * 4;
		var dY = (mouse.y - _dragStart.y) * 4;
		var toX = (_tilesOrigin.x + dX);
		var toY = (_tilesOrigin.y + dY);

		if (!released)
			TweenMax.to(_tilesDiv, .5, {x: toX, y: toY});
		else
			TweenMax.to(_tilesDiv, 4, {x: toX, y: toY, onComplete: onGridTweenDone});

	};

	//when final grid tween has completed
	function onGridTweenDone() {
		$(window).off('enterframe', updateGrid);
	};

	//show modal overlay
	function showModal(gram) {
		disableMovement();
		
		var bg = $('#modal-bg');
		$('#modal-bg').css({'display': 'block'});
		TweenMax.set(bg, {alpha: 0});
		
		_modal.css({'display': 'block'});

		if (gram.vid) {
			_modalContent = $('<div class="modal-content"><div id="flash-player"></div></div>');
		} else {
			_modalContent = $('<img class="modal-content" width="472" height="472" src="' + gram.file + '">');
		}
		
		$('#modal-icon').empty();
		$('#modal-icon').append($('<img src="'+gram.avatar+'" class="modal-avatar"/>'));
		$('#modal-icon').on('click', function(){window.open('http://instagram.com/'+gram.username);});
		
		$('#copy-handle').text(gram.username.toUpperCase());
		$('#copy-handle').on('click', function(){window.open('http://instagram.com/'+gram.username);});
		$('#copy-username').text(gram.name.toUpperCase());
		$('#copy-username').on('click', function(){window.open('http://instagram.com/'+gram.username);});
		
		if (gram.description.length > 78) gram.description = gram.description.slice(0,78) + '...';
		$('#copy-caption').text(gram.description);
		
		$('#modal-meta').html('POSTED VIA INSTAGRAM ON ' + gram.time.split(' ')[0]);
		
		TweenMax.from($('#modal-frame'), .5, {alpha: 0, onComplete:function() {}});
		
		setTimeout(function() {if (_activeTile) removeTile(_activeTile, true);}, 1000);
		
		TweenMax.from(_modalContent, .5, {alpha:0, delay:.5});
		TweenMax.from($('#modal-copy'), .5, {alpha: 0, delay:.5});
		TweenMax.from($('#modal-icon'), .5, {alpha: 0, delay:.5});
		TweenMax.from($('#modal-meta'), .5, {alpha: 0, delay:.5});
		
		$('#modal-frame').append(_modalContent);
		bg.on('click', killModal);
		
		var close = $('<img src="assets/img/close_button.png" id="modal-close">');
		$('#modal-frame').append(close);
		close.on('click', killModal);
		
		if (gram.vid) {
			var flashvars = {	
				vid:''+gram.file
			};

			var params = {
				scale:'noScale', salign:'lt', menu:'false', allowfullscreen:'true', allowscriptaccess:'always'
			};

			var attributes = {
				id:'flash-player', name:'flash-player'
			};
			
			swfobject.embedSWF("assets/swf/Player.swf", "flash-player", "472", "472", "9.0.0", "assets/js/vendor/swfobject/expressInstall.swf", flashvars, params, attributes);
		}
	};

	//hide modal overlay
	function killModal() {
		
		swfobject.removeSWF('flash-player');
		
		_modalContent.empty();
		_modalContent.remove();
		_modalContent = null;
		$('#modal-frame').empty();
		
		if (FIREFOX) {
			_modal.css({'display': 'none'});
		} else {
			TweenMax.to(_modal, .5, {x:-_viewportW, onComplete:function() {
				TweenMax.set(_modal, {x:0});
				_modal.css({'display': 'none'});
			}});
		}
		
		setTimeout(function() {
			enableMovement();
			_tilesEnabled = true;
		}, 2000);
		
		TweenMax.to(_tilesDiv, 3, {x:getX(_tilesDiv)-_viewportW*4, 
			onUpdate:function(){
				_tilesTo.x = getX(_tilesDiv);
				_tilesTo.y = getY(_tilesDiv);
			}});
		
		_activated = false;
		
		$('#modal-bg').css({'display': 'none'});
		
	};

	//build search ui
	function initSearch() {
		$('#search-holder').css({'display':'block'});
		TweenMax.from($('#search-holder'), 1, {delay:.5, alpha:0});
		var s = $('#search-input');
		s.text(SEARCH_PROMPT);
		
		s.on('focus', function() {
			if (s.text() === SEARCH_PROMPT) {
				s.text('');
			}
			
			disableMovement();
			s.on('blur', function() {if (!_modalContent) setTimeout(clearSearch, 400);});
			
			$('#search-holder').css({'background-color':'white'});
			$('#search-input').css({'color':'black'});
			$('#search').css({'border-bottom':'1px solid black'});
			$('#search-icon').attr('src', 'assets/img/search_icon_black.png');
		});
		s.on('keyup', function(e) {
			if (e.keyCode == 13) {
				s.text(s.text().replace(/(\r\n|\n|\r)/gm,''));
				setTimeout(function() { doSearch(s.text()); }, 400);
			}
		});

		$('#search-icon').css({'cursor': 'pointer'});
		$('#search-icon').on('click', function() {
			doSearch(s.text());
		});
	};
	
	function clearSearch() {
		if (_searching) return;
		if (_api.searchResults().length > 0) {
			_api.clearSearch();
		} else {
			if (!_modalContent) enableMovement();
		}

		$('#search-holder').css({'background-color':'black'});
		$('#search-input').css({'color':'white'});
		$('#search').css({'border-bottom':'1px solid white'});
		$('#search-icon').attr('src', 'assets/img/search_icon.png');

		$('#search-input').text(SEARCH_PROMPT);
	}

	//perform search
	function doSearch(s) {
		_searching = true;
		$('#search-results').css({'display':'none'});
		$('#search-input').off('blur');
		$('#search-input').blur();
	
		if (_modalContent) {
			killModal();
		}
		
		_api.search(s);
		$(_api).on('onsearch', function() {
			$('#search-results').css({'display':'block'});
			$('#result-num').text(_api.searchResults().length + 'SUBMISSIONS');
			
			enableMovement();
			
			$('#clear-search').on('click', function() {
				_searching = false;
				$('#search-results').css({'display':'none'});
				clearSearch();
			});
		});
	};

	//loading
	function showLoading() {
		TweenMax.set($('#loading'), {alpha:1});
		$('#loading').show();
	};
	
	function hideLoading() {
		TweenMax.to($('#loading'), .5, {alpha:0, onComplete:function() {
			$('#loading').hide();	
		}});
	};

	//store viewport & stage dimensions
	function onResize() {
		_stageW = $(window).width();
		_stageH = $(window).height();

		_viewportW = $(window).width() - MARGIN_LEFT - MARGIN_RIGHT;
		_viewportH = $(window).height() - MARGIN_TOP - MARGIN_BOT;

		_viewport.width(_viewportW);
		_viewport.height(_viewportH);

		TweenMax.set(_viewport, {x: MARGIN_LEFT, y: MARGIN_TOP});
	};
	
	function selectNewTransition() {
		var TRANS_NUM = 2;
		_transIndex = randInt(0,TRANS_NUM-1);
		
		setTimeout(selectNewTransition, 8000);
	}
	
	//transitions
	function transInTile(t, xRatio, yRatio, tX, tY) {
		
		if ((SAFARI || IE10) && _transIndex === 3) _transIndex = 2;
		switch (_transIndex) {
			case 0:
				//init
				var z = t.size === 1 ? randRange(2000, 4000) : t.size === 2 ? randRange(-2000, 2000) : randRange(-2000, -4000);
				TweenMax.from(t.div, randRange(2,4), {
					z:z,
					rotationX: randRange(-110,110), 
					rotationY: randRange(-110,110), 
					rotationZ: randRange(-110,110), 
					ease:Strong.easeOut,
					delay:randRange(0,2),
					onStart:function() {
						t.div.css({'visibility':'visible'});
					},
					onComplete:function() {
						t.div.css({'z-index':0});
					}
				});
				t.div.css({'z-index':Math.round(z)});
				t.div.css({'visibility':'hidden'});
			break;
			case 1:
				//firefox
				TweenMax.from(t.div, randRange(1.5,3), {
					z:randRange(-20,-20), 
					rotationX: -80 * (yRatio - .5) * -2,
					rotationY: -80 * (xRatio - .5) * 2, 
					rotationZ: 0,
					x:t.pt.x,
					y:t.pt.y,
					transformOrigin: tY + ' ' + tX + ' ' + -200 +'px',
					ease:Strong.easeOut,
					onComplete: function() { t.transitioning = false; }
				});
				t.transitioning = false;
			break;
			case 2: 
				//crazie - safari
				var z = randRange(0,800);
				TweenMax.from(t.div, randRange(.5,2), {
					z:z, 
					rotationX: -60 * (yRatio - .5) * -2, 
					rotationY: -60 * (xRatio - .5) * 2, 
					rotationZ: randRange(-30,30), 
					transformOrigin: tY + ' ' + tX + ' ' + -200 +'px'
				});
				t.transitioning = false;
			break;
			case 3: 
				//crazie 1
				var z = randRange(-800,800);
				TweenMax.from(t.div, randRange(.5,2), {
					z:z, 
					rotationX: -60 * (yRatio - .5) * -2, 
					rotationY: -60 * (xRatio - .5) * 2, 
					rotationZ: randRange(-30,30), 
					transformOrigin: tY + ' ' + tX + ' ' + -200 +'px'
				});
				t.transitioning = false;
			break;
		}
	}
};

//execute main
var m = new Main();
m.init();
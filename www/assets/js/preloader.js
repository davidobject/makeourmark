//simple jquery image preloader
function Preloader() {

	var _this = this;

	var _grams;
	var _index;
	var _quality;
	var _urls;
	
	this.preloadURLS = function(urls) {
		_urls = urls; 
		if (_urls) {
			_index = 0;
			loadNextURL();
		}
	}
	
	function loadNextURL() {
		var url = _urls[_index];
		var img = $('<img src="'+url+'"/>');
		img.on('load', onLoadNextURL);
	};
	
	function onLoadNextURL(e) {
		if (++_index === _urls.length) {
			$(_this).trigger('preloaded');
		} else {
			loadNextURL();
		}
	};
	
	this.preloadGrams = function(grams, quality) {
		_quality = quality;
		
		if (grams && grams.length > 0) {
			_grams = grams;
			_index = 0;
			loadNextGram();
		}
	};
	
	function loadNextGram() {
		var url = _grams[_index].imgURLS[_quality];
		var img = $('<img src="'+url+'"/>');
		img.on('load', onLoadNextGram);
	};
	
	function onLoadNextGram(e) {
		var gram = _grams[_index];
		gram.img[_quality] = e.target;
		
		if (++_index === _grams.length) {
			$(_this).trigger('preloaded');
		} else {
			loadNextGram();
		}
	};
	
};
function Vec2(x,y) {
	this.x = x;
	this.y = y;
	this.toString = function() {
		return '[Vec2: ' + this.x + ', ' + this.y + ']';
	};
}

function Vec3(x,y,z) {
	this.x = x;
	this.y = y;
	this.z = z;
	this.toString = function() {
		return '[Vec3: ' + this.x + ', ' + this.y + ', ' + this.z + ']';
	};
}

function Rect(x,y,w,h) {
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
	this.toString = function() {
		return '[Rect: ' + this.x + ', ' +this.y + ', ' + this.w + ', ' + this.h + ', ' + ']';
	};
}

function Quad(pt, tile) {
	this.pt = pt;
	this.tile = tile;
	this.toString = function() {
		return '[Quad: ' + this.pt + ' tile: ' + this.tile + ']';
	};
}

function Tile(pt, size, div) {
	this.pt = pt;
	this.size = size;
	this.div = div;
	this.ctx;
	
	this.quads = [];
	this.width = size*QUAD_W;
	this.height = size*QUAD_H;
	this.gram;
	
	this.img;
	this.border;
	this.overlay;

	this.transitioning = false;
	
	
	this.toString = function() {
		return '[Tile: ' + this.pt + ' size: ' + this.size + ']';
	};
}

//instagram post
function Gram() {
	this.id;
	this.time;
	this.title;
	this.description;
	this.keywords;
	this.name;
	this.username;
	this.avatar;
	this.img = {};
	this.imgURLS = {};
	this.imgURLS.thumb;
	this.imgURLS.large;
	this.imgURLS.medium;
	this.imgURLS.small;
	this.file;
	this.vid;
}
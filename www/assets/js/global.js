//global constants
var DEBUG = false;
var PLAY_SOUND = true;

//insta dimensions: thumb: 150x150, medium: 306x306, large: 612x612

var QUAD_W = 75;
var QUAD_H = 75;
var MAX_SIZE = 3;


//Browsers
var IE6 = false /*@cc_on || @_jscript_version < 5.7 @*/
var IE7 = (document.all && !window.opera && window.XMLHttpRequest && navigator.userAgent.toString().toLowerCase().indexOf('trident/4.0') == -1) ? true : false;
var IE8 = (navigator.userAgent.toString().toLowerCase().indexOf('trident/4.0') != -1);
var IE9 = navigator.userAgent.toString().toLowerCase().indexOf("trident/5")>-1;
var IE10 = navigator.userAgent.toString().toLowerCase().indexOf("trident/6")>-1;
var SAFARI = (navigator.userAgent.toString().toLowerCase().indexOf("safari") != -1) && (navigator.userAgent.toString().toLowerCase().indexOf("chrome") == -1);
var FIREFOX = (navigator.userAgent.toString().toLowerCase().indexOf("firefox") != -1);
var CHROME = (navigator.userAgent.toString().toLowerCase().indexOf("chrome") != -1);
var MOBILE_SAFARI = ((navigator.userAgent.toString().toLowerCase().indexOf("iphone")!=-1) || (navigator.userAgent.toString().toLowerCase().indexOf("ipod")!=-1) || (navigator.userAgent.toString().toLowerCase().indexOf("ipad")!=-1)) ? true : false;
 
//Platforms
var MAC = (navigator.userAgent.toString().toLowerCase().indexOf("mac")!=-1) ? true: false;
var WINDOWS = (navigator.appVersion.indexOf("Win")!=-1) ? true : false;
var LINUX = (navigator.appVersion.indexOf("Linux")!=-1) ? true : false;
var UNIX = (navigator.appVersion.indexOf("X11")!=-1) ? true : false;
